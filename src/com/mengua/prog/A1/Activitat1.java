package com.mengua.prog.A1;

public class Activitat1 {

    public static void main(String[] args){

        Televisio tele1 = new Televisio("Panasonic");

        tele1.powerOn();

        tele1.setChannelUp();

        tele1.setChannelDown();

        tele1.setVolumeUp();

        tele1.setVolumeDown();

        tele1.powerOff();

    }

}
