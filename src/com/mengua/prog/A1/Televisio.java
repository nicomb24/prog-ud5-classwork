package com.mengua.prog.A1;

public class Televisio {

    private boolean status;

    private int channel;

    private int volume;

    private String marca;

    public Televisio(String marca) {
        this.marca = marca;
        this.status = false;
        this.volume = 10;
        this.channel = 1;
    }

    public void powerOn(){
        this.status=true;
        System.out.printf("El %s esta ences", marca);
    }

    public void powerOff(){
        this.status=false;
        System.out.printf("\nEl %s esta apagat", marca);
    }

    public void setVolumeUp(){
        this.volume+=1;
        System.out.printf("\nEl volum es %d", volume);
    }

    public void setVolumeDown(){
        this.volume-=1;
        System.out.printf("\nEl volum es %d", volume);
    }

    public void setChannelUp(){
        this.channel+=1;
        System.out.printf("\nEl canal es %d", channel);
    }

    public void setChannelDown(){
        this.channel-=1;
        System.out.printf("\nEl canal es %d", channel);
    }

}
