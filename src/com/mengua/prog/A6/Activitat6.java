package com.mengua.prog.A6;

public class Activitat6 {

    public static void main (String[] args){

        Rellotge rellotge1 = new Rellotge(23,50,07);

        rellotge1.showHora24H();

        rellotge1.showHora12H();

        rellotge1.canviarHoraMinuts(10,34);

        rellotge1.showHora24H();

        rellotge1.canviarHoraMinutsSegons(23,34,12);

        rellotge1.showHora24H();

    }

}
