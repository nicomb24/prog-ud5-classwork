package com.mengua.prog.A6;

public class Rellotge {

    private int hora;

    private int minuts;

    private int segons;

    public Rellotge() {
        this.hora = 0;
        this.minuts = 0;
        this.segons = 0;
    }

    public Rellotge(int hora, int minuts, int segons) {
        this.hora = hora;
        this.minuts = minuts;
        this.segons = segons;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public void setMinuts(int minuts) {
        this.minuts = minuts;
    }

    public void setSegons(int segons) {
        this.segons = segons;
    }

    public int getHora() {
        return hora;
    }

    public int getMinuts() {
        return minuts;
    }

    public int getSegons() {
        return segons;
    }

    public void showHora24H(){

        System.out.printf("%d:%d:%d\n", hora, minuts, segons);

    }

    public void showHora12H(){

        if (hora > 12){
            hora -= 12;
            System.out.printf("%d:%d:%d PM\n", hora, minuts, segons);
            hora += 12;
        }else {

            System.out.printf("%d:%d:%d AM\n", hora, minuts, segons);

        }
    }

    public void canviarHoraMinuts(int hora, int minuts){

        setHora(hora);
        setMinuts(minuts);
        setSegons(0);

    }

    public void canviarHoraMinutsSegons(int hora, int minuts, int segons){

        setHora(hora);
        setMinuts(minuts);
        setSegons(segons);

    }
}
