package com.mengua.prog.A10;

import com.mengua.prog.A9.Persona;

public class Cuenta {

    private int numCuenta;

    private int interes;

    private float saldo;

    private Persona cliente;

    public Cuenta(int numCuenta, int interes, Persona cliente) {
        this.numCuenta = numCuenta;
        this.interes = interes;
        this.cliente = cliente;
        this.saldo = 0;
    }

    public void ingresar(float cantidad){
        saldo += cantidad;
    }

    public void reintegrar(float cantidad){
        if (saldo >= cantidad) {
            saldo -= cantidad;
        }
    }

    public void transferencia(Cuenta cuentaDestino, float cantidad){

        reintegrar(cantidad);
        cuentaDestino.ingresar(cantidad);

    }

    @Override
    public String toString() {
        return cliente + "\nCuenta: " + numCuenta + "\nSaldo: " + saldo + "€\n";
    }

}
