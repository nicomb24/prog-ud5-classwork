package com.mengua.prog.A10;

import com.mengua.prog.A8.Fecha;
import com.mengua.prog.A9.Persona;

public class Activitat10 {

    public static void main (String[]args){

        Fecha nacimientoNico = new Fecha(24,9,1994);
        Persona nico = new Persona("Nicolas", 24, "74017857Q", nacimientoNico);
        Cuenta cuentaNico = new Cuenta(1234, 5, nico);

        Fecha nacimientoPepe = new Fecha(3,10,1956);
        Persona pepe = new Persona("Pepe", 62, "74080526X", nacimientoPepe);
        Cuenta cuentaPepe = new Cuenta(2345, 5, pepe);

        cuentaNico.ingresar(4000);

        cuentaPepe.ingresar(10000);

        cuentaPepe.transferencia(cuentaNico, 1000);

        System.out.print(cuentaNico);
        System.out.print(cuentaPepe);

    }

}
