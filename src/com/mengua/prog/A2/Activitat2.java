package com.mengua.prog.A2;

public class Activitat2 {

    public static void main (String[] args){

        Cotxe audi = new Cotxe();

        Cotxe volkswagen = new Cotxe("Scirocco", "Blau", true, "4272JPX", TipusCotxe.ESPORTIU, 2018, true);

        audi.mostrarInfo();

        volkswagen.mostrarInfo();

    }

}
