package com.mengua.prog.A2;

public class Cotxe {

    private String model;

    private String color;

    private boolean pinturaMetalitzada;

    private String matricula;

    private TipusCotxe tipusCotxe;

    private int anyFabricacio;

    private boolean esTotRisc;

    public Cotxe(String model, String color, boolean pinturaMetalitzada, String matricula, TipusCotxe tipusCotxe, int anyFabricacio, boolean esTotRisc) {
        this.model = model;
        this.color = color;
        this.pinturaMetalitzada = pinturaMetalitzada;
        this.matricula = matricula;
        this.tipusCotxe = tipusCotxe;
        this.anyFabricacio = anyFabricacio;
        this.esTotRisc = esTotRisc;
    }

    public Cotxe() {
        this.model = "Audi";
        this.color = "Blanc";
        this.pinturaMetalitzada = true;
        this.matricula = "0000ABC";
        this.tipusCotxe = TipusCotxe.UTILITARI;
        this.anyFabricacio = 2018;
        this.esTotRisc = true;
    }

    public void mostrarInfo(){

        System.out.printf("El model es %s i el color %s i es de tipus %s\n", model, color, tipusCotxe);

    }
}
