package com.mengua.prog.A9;

import com.mengua.prog.A8.Fecha;

public class Persona {

    private String nombre;

    private int edad;

    private String dni;

    private Fecha fechaNacimiento;

    public Persona(String nombre, int edad, String dni, Fecha fechaNacimiento) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return nombre + ", " + edad + " años con DNI " + dni;
    }

}
