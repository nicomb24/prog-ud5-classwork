package com.mengua.prog.A3;

public class Assignatura {

     private String nom;

     private int codi;

     private String curs;

    public Assignatura() {
        this.nom = "Nom assignatura";
        this.codi = 0000;
        this.curs = "Curs assignatura";
    }

    public void setNom(String nom){

        this.nom = nom;

    }

    public void setCodi(int codi){

        this.codi = codi;

    }

    public void setCurs(String curs){

        this.curs = curs;

    }

    public void showAssignatura(){

        System.out.print(
                "Nom: " + this.nom +
                "\nCodi: " + this.codi +
                "\nCurs: " + this.curs
        );

    }
}
