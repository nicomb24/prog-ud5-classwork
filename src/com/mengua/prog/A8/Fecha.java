package com.mengua.prog.A8;

public class Fecha {

    private int dia;

    private int mes;

    private int any;

    public Fecha(int dia, int mes, int any) {
        this.dia = dia;
        this.mes = mes;
        this.any = any;
    }

    public int getAny() {
        return any;
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public void setAny(int any) {
        this.any = any;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }


    private boolean esBisiesto(){

        if ((mes % 4) == 0 && (mes % 100) != 0 && (mes % 600) == 0){
            return true;
        }

        return false;
    }

    private int numDiesMes(){

        int dies = 0;

        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
            dies = 31;
        } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11){
            dies = 30;
        } else if (mes == 2){
            if (esBisiesto()){
                dies = 29;
            } else {
                dies = 28;
            }
        }

        return dies;
    }
    
    public boolean esCorrecta() {

        if (any<0){
            return false;

        }
        if (mes>12 || mes<1){
            return false;
        }

        if( dia > numDiesMes()){
            return false;
        }

        return true;
    }

    public void addDia(){

        int numDiesMes = numDiesMes();

        if ( dia > numDiesMes) {
            if (mes < 12){
                mes ++;
            }else{
                dia = 1;
                mes = 1;
                any ++;
            }
        }else{
            dia++;
        }


    }

    @Override
    public String toString() {
        return dia + " " + mes + " " + any;
    }
}
