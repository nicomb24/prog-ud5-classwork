package com.mengua.prog.A8;

public class Activitat8 {

    public static void main (String[] args){

        Fecha fecha1 = new Fecha(24,9,1994);
        Fecha fecha2 = new Fecha(32,1,12);
        Fecha fecha3 = new Fecha(30,2,2000);
        Fecha fecha4 = new Fecha(5,4,0);

        fecha1.esCorrecta();
        fecha2.esCorrecta();
        fecha3.esCorrecta();
        fecha4.esCorrecta();

        fecha1.addDia();

        System.out.print(fecha1);



    }

}
