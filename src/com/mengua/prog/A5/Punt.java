package com.mengua.prog.A5;

public class Punt {

    private double coordX;

    private double coordY;

    public Punt(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public double getCoordX() {
        return coordX;
    }

    public double getCoordY() {
        return coordY;
    }

    public float distanciaPunts(Punt punt){

        return  (float) Math.sqrt((Math.pow((punt.getCoordX() - coordX),2)) + (Math.pow((punt.getCoordY() - coordY),2)));

    }

    public void showInfo(){

        System.out.printf("Coordenada X: %f \nCoordenada Y: %f", this.coordX, this.coordY);

    }
}
