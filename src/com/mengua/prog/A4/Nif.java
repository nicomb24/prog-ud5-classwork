package com.mengua.prog.A4;

public class Nif {

    private int numDni;

    private char lletra;

    public Nif(int numDni) {
        this.numDni = numDni;
        this.lletra = calculNif(numDni);
    }

    private static char calculNif(int numDni){

        String calcuLletra = "TRWAGMYFPDXBNJZSQVHLCKE";
        char lletra = calcuLletra.charAt(numDni%23);
        return lletra;

    }

    public void showNif(){

        System.out.printf("El NIF és: %d-%c", this.numDni, this.lletra  );

    }
}
