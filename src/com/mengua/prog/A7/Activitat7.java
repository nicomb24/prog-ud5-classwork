package com.mengua.prog.A7;

public class Activitat7 {

    public static void main (String[] args){

        // Se crean 4 fracciones
        Fraccio f1 = new Fraccio(1, 4); // Fracción 1/4
        Fraccio f2 = new Fraccio(1, 2); // Fracción 1/2
        Fraccio f3 = new Fraccio(0,1); // Fracción 0/1
        Fraccio f4 = new Fraccio(4,1); // Fracción 4/1
        // operaciones aritméticas con esas fracciones
        Fraccio suma = f1.sumar(f2);
        Fraccio resta = f1.restar(f3);
        Fraccio producto = f1.multiplicar(f4);
        Fraccio cociente = f1.dividir(f2);
        //mostrar resultados
        System.out.println(f1 + " + " + f2 + " = " + suma);
        System.out.println(f1 + " - " + f3 + " = " + resta);
        System.out.println(f1 + " * " + f4 + " = " + producto);
        System.out.println(f1 + " / " + f2 + " = " + cociente);
    }

}
