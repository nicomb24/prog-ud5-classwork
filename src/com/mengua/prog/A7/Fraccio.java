package com.mengua.prog.A7;

public class Fraccio {

    private int numerador;

    private int denominador;

    public Fraccio(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public int getDenominador() {
        return denominador;
    }

    public int getNumerador() {
        return numerador;
    }

    private int mcm(int num1, int num2){

        for (int i = 1; i < num1 * num2; i++) {
            if (i % num1 == 0 && i % num2 == 0){
                return i;
            }
        }

        return num1 * num2;

    }

    public Fraccio multiplicar(Fraccio fraccio){

        Fraccio result = new Fraccio(numerador * fraccio.getNumerador(), denominador * fraccio.getDenominador());
        result.simplificar();
        return result;

    }

    public Fraccio dividir(Fraccio fraccio){

        Fraccio result = new Fraccio(numerador * fraccio.getDenominador(), denominador * fraccio.getNumerador());
        result.simplificar();
        return result;

    }

    public Fraccio sumar(Fraccio fraccio){

        int mcm = mcm(denominador, fraccio.getDenominador());

        int resNominador = (numerador * (mcm / denominador) + (fraccio.getNumerador() * (mcm / fraccio.getDenominador())));

        Fraccio result = new Fraccio(resNominador,mcm);
        result.simplificar();
        return result;

    }

    public Fraccio restar(Fraccio fraccio){

        int mcm = mcm(denominador, fraccio.getDenominador());

        int resNominador = (numerador * (mcm / denominador) - (fraccio.getNumerador() * (mcm / fraccio.getDenominador())));

        Fraccio result = new Fraccio(resNominador,mcm);
        result.simplificar();
        return result;

    }

    public void simplificar(){

        for (int i = 2; i <= Math.min(numerador, denominador); i++ ){

            if (i % numerador == 0 && i % denominador == 0){
                numerador /= i;
                denominador /=i;
                i=1;
            }

        }

    }

    @Override
    public String toString(){
        return numerador + "/" + denominador + "\n";
    }
}
